import $ from 'jquery';
import createPerson from './createPerson';

export default function randerPerson(response) {
  $('#name').html(createPerson(response).name);
  $('#age').html(createPerson(response).age);
  $('#description').html(createPerson(response).description);
  const personDescription = document.getElementById('description');
  personDescription.innerText = createPerson(response).description;
}
