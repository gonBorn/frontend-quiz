export class Person {
  constructor(name, age, description, education) {
    this.name = name;
    this.age = age;
    this.description = description;
    this.education = education.map(item => {
      const { year, title, description } = item;
      return new Education(year, title, description);
    });
  }
}

export class Education {
  constructor(year, title, description) {
    this.year = year;
    this.title = title;
    this.description = description;
  }
}
