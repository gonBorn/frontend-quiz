import fetchData from './fetchData';
import createPerson from './createPerson';
import randerPerson from './randerPerson';
import renderEducation from './renderEducation';

fetchData()
  .then(response => {
    // eslint-disable-next-line no-console
    console.log(createPerson(response));
    randerPerson(response);
    renderEducation(response);
  })
  .catch(error => error);
