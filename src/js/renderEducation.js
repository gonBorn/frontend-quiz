import $ from 'jquery';
import createPerson from './createPerson';

export default function renderEducation(response) {
  const array = createPerson(response).education;
  array.forEach(item => {
    $('ul').append(
      `<li><h4>${item.year}</h4><div><h4>${item.title}</h4><p>${item.description}</p></div></li>`
    );
  });
}
